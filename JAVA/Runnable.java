import PaymentOptions.*;

public class Runnable {

	public static void main(String [] args) {
		PaymentOption pOpt = new PaymentOption(0.0, 10.0);
		Cash cashPmt = new Cash(37.99);
		Check checkPmt = new Check(22.87, "Novalândia");
		CreditCard ccPmt = new CreditCard(5.8, "55863632", 11, 2016);

		System.out.println(  "PaymentOption payment value: " + pOpt.getTotal() + "\nVerify: " + pOpt.verify()
				+ "\nCash payment value: " + cashPmt.getTotal() + "\nVerify: " + cashPmt.verify()
				+ "\nCheck payment value: " + checkPmt.getTotal() + "\nVerify: " + checkPmt.verify()
				+ "\nCreditCard payment value: " + ccPmt.getTotal() + "\nVerify: " + ccPmt.verify() );
	}

}
