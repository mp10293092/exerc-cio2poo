package PaymentOptions;

public class PaymentOption {

	//private:
			private double value;
			private double discount;

	//public:
			public PaymentOption(double newDiscount, double newValue) {
				setDiscount(newDiscount);
				setValue(newValue);
			}

			//Métodos Getter
			public double getTotal() {
				return value - discount;
			}

			//Métodos Setter
			public void setDiscount(double newDiscount) {
				if ( newDiscount < 0.0 ) {
					newDiscount = 0.0;
					System.err.println( "Erro! O desconto não pode ser um número negativo\nAtribuindo valor padrão: "
 											+ newDiscount );
				}
				discount = newDiscount;
			}
			public void setValue(double newValue) {
				if ( newValue < 0.0 ) {
					newValue = 0.0;
					System.err.println( "Erro! O valor não pode ser negativo\nAtribuindo valor padrão: "
											+ newValue  );
				}
				value = newValue;
			}

			public boolean verify() {
				return true;
			}

}
