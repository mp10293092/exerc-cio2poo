package PaymentOptions;

import java.util.Random;

public class Check extends PaymentOption {

	//private:
	private String bank;

	//public:
	public Check(double newValue, String bankName) {
		super(0.0, newValue);
		bank = bankName;
	}

	public boolean verify() {
		Random dice = new Random();
		return (dice.nextDouble() < 0.98);
	}

}
