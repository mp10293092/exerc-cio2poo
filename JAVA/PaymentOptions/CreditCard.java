package PaymentOptions;

import java.util.Random;

public class CreditCard extends PaymentOption {

	//private:
	private String cardNumber;
	private int expMonth;
	private int expYear;

	//public:
	public CreditCard(double newValue, String newCardNumber, int newExpMonth, int newExpYear) {
			super(0.0, newValue);
			cardNumber = newCardNumber;
			expMonth = newExpMonth;
			expYear = newExpYear;
	}

	public boolean verify() {
		Random dice = new Random();
		return (dice.nextDouble() < 0.93);
	}

}
