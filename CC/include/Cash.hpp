#ifndef CASH_HPP
#define CASH_HPP

#include "PaymentOption.hpp"

class Cash : public PaymentOption {

	public:
			Cash(double newValue);

};

#endif
