#ifndef CREDITCARD_HPP
#define CREDITCARD_HPP

#include "PaymentOption.hpp"

#include <string>

using std::string;

class CreditCard : public PaymentOption {

	private:
			string cardNumber;
			int expMonth;
			int expYear;

	protected:

	public:
			CreditCard(double newValue, string newCardNumber, int newExpMonth, int newExpYear);

			bool verify();

};

#endif
