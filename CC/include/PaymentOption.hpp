#ifndef PAYMENTOPTION_HPP
#define PAYMENTOPTION_HPP

class PaymentOption {

	private:
			double value;
			double discount;

	protected:

	public:
			PaymentOption(double newDiscount, double newValue);

			//Métodos Getter
			double getTotal();

			//Métodos Setter
			void setDiscount(double newDiscount);
			void setValue(double newValue);

			virtual bool verify();

};

#endif
