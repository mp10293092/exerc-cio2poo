#ifndef CHECK_HPP
#define CHECK_HPP

#include "PaymentOption.hpp"

#include <string>

using std::string;

class Check : public PaymentOption {

	private:
			string bank;

	protected:

	public:
			Check(double newValue, string bankName);
			bool verify();

};

#endif
