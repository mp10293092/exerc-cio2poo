#include <Check.hpp>

#include <cstdlib>
#include <ctime>
#include <string>

using std::string;

Check::Check(double newValue, string bankName) : PaymentOption(0.0, newValue) {
	srand( time(NULL) );
	bank = bankName;
}

bool Check::verify() {
	return ( ( rand() / (double)(RAND_MAX) ) < 0.98);
}
