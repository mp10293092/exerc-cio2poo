#include <PaymentOption.hpp>
#include <Cash.hpp>
#include <Check.hpp>
#include <CreditCard.hpp>

#include <iostream>

using std::cout;
using std::endl;

int main() {

	PaymentOption pOpt = PaymentOption(0.0, 10.0);
	Cash cashPmt = Cash(37.99);
	Check checkPmt = Check(22.87, "Novalândia");
	CreditCard ccPmt = CreditCard(5.8, "55863632", 11, 2016);

	cout << "PaymentOption payment value: " << pOpt.getTotal() << "\nVerify: " << pOpt.verify()
			<< "\nCash payment value: " << cashPmt.getTotal() << "\nVerify: " << cashPmt.verify()
			<< "\nCheck payment value: " << checkPmt.getTotal() << "\nVerify: " << checkPmt.verify()
			<< "\nCreditCard payment value: " << ccPmt.getTotal() << "\nVerify: " << ccPmt.verify()
			<< endl;


	return 0;
}
