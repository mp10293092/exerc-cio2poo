#include <CreditCard.hpp>

#include <cstdlib>
#include <string>

using std::string;

CreditCard::CreditCard(double newValue, string newCardNumber, int newExpMonth, int newExpYear)
			: PaymentOption(0.0, newValue) {
		cardNumber = newCardNumber;
		expMonth = newExpMonth;
		expYear = newExpYear;
}

bool CreditCard::verify() {
	double dice = rand() / (double)(RAND_MAX);
	return (dice < 0.93);
}
