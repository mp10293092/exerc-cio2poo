#include <PaymentOption.hpp>

#include <iostream>

using namespace std;

PaymentOption::PaymentOption(double newDiscount, double newValue) {
	setDiscount(newDiscount);
	setValue(newValue);
}

//Métodos Getter
double PaymentOption::getTotal() {
	return value - discount;
}

//Métodos Setter
void PaymentOption::setDiscount(double newDiscount) {
	if ( newDiscount < 0.0 ) {
		newDiscount = 0.0;
		cerr << "Erro! O desconto não pode ser um número negativo\nAtribuindo valor padrão: "
				<< newDiscount << endl;
	}
	discount = newDiscount;
}

void PaymentOption::setValue(double newValue) {
	if ( newValue < 0.0 ) {
		newValue = 0.0;
		cerr << "Erro! O valor não pode ser negativo\nAtribuindo valor padrão: "
				<< newValue << endl;
	}
	value = newValue;
}

bool PaymentOption::verify() {
	return true;
}
